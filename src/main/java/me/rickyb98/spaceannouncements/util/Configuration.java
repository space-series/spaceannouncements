package me.rickyb98.spaceannouncements.util;

import com.yakovliam.spaceapi.config.adapter.ConfigurationAdapter;
import com.yakovliam.spaceapi.config.keys.ConfigKey;
import com.yakovliam.spaceapi.config.keys.ConfigKeyTypes;
import me.rickyb98.spaceannouncements.storage.AbstractAnnouncementLoader;

import static me.rickyb98.spaceannouncements.storage.AbstractAnnouncementLoader.Type.MYSQL;

public class Configuration {

    public static final ConfigKey<String> DB_HOSTNAME = ConfigKeyTypes.stringKey("database.hostname", "localhost");
    public static final ConfigKey<Integer> DB_PORT = ConfigKeyTypes.integerKey("database.port", 3306);
    public static final ConfigKey<String> DB_USERNAME = ConfigKeyTypes.stringKey("database.username", "mysqlUsername");
    public static final ConfigKey<String> DB_PASSWORD = ConfigKeyTypes.stringKey("database.password", "mysqlPassword");
    public static final ConfigKey<String> DB_NAME = ConfigKeyTypes.stringKey("database.name", "mysqlDatabase");

    public static final ConfigKey<String> SERVER_NAME = ConfigKeyTypes.stringKey("server-name", "global");

    public static final ConfigKey<AbstractAnnouncementLoader.Type> LOADER_TYPE = ConfigKeyTypes.enumKey("loader-type", MYSQL);

    private ConfigurationAdapter adapter;

    public Configuration(ConfigurationAdapter adapter) {
        this.adapter = adapter;
    }

    public ConfigurationAdapter getAdapter() {
        return adapter;
    }
}
