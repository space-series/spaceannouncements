package me.rickyb98.spaceannouncements;

import com.yakovliam.spaceapi.abstraction.BungeePlugin;
import com.yakovliam.spaceapi.config.BungeeConfig;
import com.yakovliam.spaceapi.config.adapter.BungeeConfigurateAdapter;
import me.rickyb98.spaceannouncements.util.Configuration;
import net.md_5.bungee.api.plugin.Plugin;

public class SpaceAnnouncementsBungee extends Plugin {

    private static SpaceAnnouncementsBungee instance;

    public SpaceAnnouncementsBungee() {
        instance = this;
    }

    public static SpaceAnnouncementsBungee getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        super.onEnable();

        BungeeConfig config = new BungeeConfig(SpaceAnnouncementsBungee.getInstance(), "config.yml");
        SpaceAnnouncements instance = new SpaceAnnouncements(
                new BungeePlugin(SpaceAnnouncementsBungee.getInstance()),
                new Configuration(new BungeeConfigurateAdapter(config)),
                new BungeeConfigurateAdapter(new BungeeConfig(this, "announcements.yml"))
        );

        SpaceAnnouncements.setInstance(instance);
        instance.enable();

        // todo add metrics
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
