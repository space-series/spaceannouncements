package me.rickyb98.spaceannouncements;

import com.yakovliam.spaceapi.abstraction.AbstractPlugin;
import com.yakovliam.spaceapi.abstraction.BukkitPlugin;
import com.yakovliam.spaceapi.abstraction.BungeePlugin;
import com.yakovliam.spaceapi.config.adapter.ConfigurationAdapter;
import me.rickyb98.spaceannouncements.command.ManualAnnounceCommand;
import me.rickyb98.spaceannouncements.handler.AnnouncementHandler;
import me.rickyb98.spaceannouncements.handler.BukkitAnnouncementHandler;
import me.rickyb98.spaceannouncements.handler.BungeeAnnouncementHandler;
import me.rickyb98.spaceannouncements.storage.FileAnnouncementLoader;
import me.rickyb98.spaceannouncements.storage.MySQLAnnouncementLoader;
import me.rickyb98.spaceannouncements.util.Configuration;

import static me.rickyb98.spaceannouncements.util.Configuration.LOADER_TYPE;

public class SpaceAnnouncements {

    private static SpaceAnnouncements instance;

    public static SpaceAnnouncements getInstance() {
        return instance;
    }

    static void setInstance(SpaceAnnouncements instance) {
        SpaceAnnouncements.instance = instance;
    }

    private AbstractPlugin plugin;
    private Configuration configuration;
    private AnnouncementHandler handler;

    private final ConfigurationAdapter announcementFile;

    SpaceAnnouncements(AbstractPlugin plugin, Configuration configuration, ConfigurationAdapter announcementFile) {
        this.plugin = plugin;
        this.configuration = configuration;
        this.announcementFile = announcementFile;
    }

    void enable() {
        // load commands
        new ManualAnnounceCommand();

        // pick loader
        ConfigurationAdapter adapter = configuration.getAdapter();
        if (plugin instanceof BukkitPlugin) {
            switch (LOADER_TYPE.get(adapter)) {
                case FILE:
                    handler = new BukkitAnnouncementHandler(new FileAnnouncementLoader(announcementFile));
                    break;
                case MYSQL:
                    handler = new BukkitAnnouncementHandler(new MySQLAnnouncementLoader());
                    break;
            }
        } else if (plugin instanceof BungeePlugin) {
            switch (LOADER_TYPE.get(adapter)) {
                case FILE:
                    handler = new BungeeAnnouncementHandler(new FileAnnouncementLoader(announcementFile));
                    break;
                case MYSQL:
                    handler = new BungeeAnnouncementHandler(new MySQLAnnouncementLoader());
                    break;
            }
        }

    }

    public AnnouncementHandler getHandler() {
        return handler;
    }

    public AbstractPlugin getPlugin() {
        return plugin;
    }

    public Configuration getConfiguration() {
        return configuration;
    }
}
