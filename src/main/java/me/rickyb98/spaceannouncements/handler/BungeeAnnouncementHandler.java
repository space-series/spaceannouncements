package me.rickyb98.spaceannouncements.handler;

import me.rickyb98.spaceannouncements.SpaceAnnouncementsBungee;
import me.rickyb98.spaceannouncements.storage.AbstractAnnouncementLoader;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class BungeeAnnouncementHandler extends AnnouncementHandler {
    private int current = -1;

    public BungeeAnnouncementHandler(AbstractAnnouncementLoader loader) {
        super(loader);
        ProxyServer.getInstance().getScheduler().schedule(SpaceAnnouncementsBungee.getInstance(), () ->
                        announcements = loader.loadAnnouncements(),
                0L, 60L, TimeUnit.SECONDS);
    }

    @Override
    public void sendNextAnnouncement() {
        ++current;

        ProxyServer.getInstance().getServers().forEach((serverName, server) -> {
            Set<Announcement> set = announcements.stream()
                    .filter(a -> Arrays.stream(a.getServers()).anyMatch(s -> s.equalsIgnoreCase(serverName)) ||
                            a.isGlobal())
                    .collect(Collectors.toSet());
            if (set.size() == 0) return;

            set.stream().skip(current % set.size()).findFirst().ifPresent(a ->
                    server.getPlayers().forEach(p -> {
                                for (String text : a.getText().split("\\n")) {
                                    p.sendMessage(TextComponent.fromLegacyText(
                                            ChatColor.translateAlternateColorCodes('&', text)
                                    ));
                                }
                            }
                    )
            );
        });
    }
}
