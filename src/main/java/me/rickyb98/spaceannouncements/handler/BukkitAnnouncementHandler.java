package me.rickyb98.spaceannouncements.handler;

import com.yakovliam.spaceapi.config.adapter.ConfigurationAdapter;
import me.rickyb98.spaceannouncements.SpaceAnnouncements;
import me.rickyb98.spaceannouncements.SpaceAnnouncementsBukkit;
import me.rickyb98.spaceannouncements.storage.AbstractAnnouncementLoader;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static me.rickyb98.spaceannouncements.util.Configuration.SERVER_NAME;

public class BukkitAnnouncementHandler extends AnnouncementHandler {

    private int current = 0;

    public BukkitAnnouncementHandler(AbstractAnnouncementLoader loader) {
        super(loader);

        Bukkit.getScheduler().runTaskTimerAsynchronously(SpaceAnnouncementsBukkit.getInstance(),
                () -> announcements = loader.loadAnnouncements(),
                0L, 1200L);
    }

    @Override
    public void sendNextAnnouncement() {
        ++current;

        ConfigurationAdapter adapter = SpaceAnnouncements.getInstance().getConfiguration().getAdapter();
        Set<Announcement> set = announcements.stream()
                .filter(a -> Arrays.stream(a.getServers()).anyMatch(s -> s.equalsIgnoreCase(SERVER_NAME.get(adapter))) ||
                        a.isGlobal())
                .collect(Collectors.toSet());
        if (set.size() == 0) return;

        set.stream().skip(current % set.size()).findFirst().ifPresent(a ->
                Bukkit.getOnlinePlayers().forEach(p -> {
                            for (String text : a.getText().split("\\n")) {
                                p.spigot().sendMessage(TextComponent.fromLegacyText(
                                        ChatColor.translateAlternateColorCodes('&', text)
                                ));
                            }
                        }
                )
        );
    }
}
