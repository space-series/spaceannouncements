package me.rickyb98.spaceannouncements.handler;

import java.util.Objects;

public class Announcement implements Comparable<Announcement> {

    private String handle;
    private String text;
    private String[] servers;
    private boolean isGlobal;

    public Announcement(String handle, String text, boolean isGlobal, String[] servers) {
        this.handle = handle;
        this.text = text;
        this.isGlobal = isGlobal;
        this.servers = servers;
    }

    public String getHandle() {
        return handle;
    }

    public String getText() {
        return text;
    }

    public String[] getServers() {
        return servers;
    }

    public boolean isGlobal() {
        return isGlobal;
    }

    @Override
    public int compareTo(Announcement announcement) {
        return this.handle.compareToIgnoreCase(announcement.handle);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Announcement that = (Announcement) o;
        return handle.equals(that.handle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(handle);
    }
}
