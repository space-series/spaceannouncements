package me.rickyb98.spaceannouncements.handler;

import me.rickyb98.spaceannouncements.storage.AbstractAnnouncementLoader;

import java.util.Set;
import java.util.TreeSet;

public abstract class AnnouncementHandler {

    protected final AbstractAnnouncementLoader loader;

    protected Set<Announcement> announcements = new TreeSet<>();

    public AnnouncementHandler(AbstractAnnouncementLoader loader) {
        this.loader = loader;
    }

    public abstract void sendNextAnnouncement();

}
