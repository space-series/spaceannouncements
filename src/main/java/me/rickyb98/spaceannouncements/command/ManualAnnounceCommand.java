package me.rickyb98.spaceannouncements.command;

import com.yakovliam.spaceapi.command.Command;
import com.yakovliam.spaceapi.command.Permissible;
import com.yakovliam.spaceapi.command.SpaceCommandSender;
import me.rickyb98.spaceannouncements.SpaceAnnouncements;

@Permissible("space.announcements.manualannounce")
public class ManualAnnounceCommand extends Command {

    public ManualAnnounceCommand() {
        super(SpaceAnnouncements.getInstance().getPlugin(), "manualannounce",
                "Broadcasts next annonucement", "/manualannounce");
    }

    @Override
    public void onCommand(SpaceCommandSender spaceCommandSender, String s, String... args) {
        SpaceAnnouncements.getInstance().getHandler().sendNextAnnouncement();
    }
}
