package me.rickyb98.spaceannouncements;

import com.yakovliam.spaceapi.abstraction.BukkitPlugin;
import com.yakovliam.spaceapi.config.BukkitConfig;
import com.yakovliam.spaceapi.config.adapter.BukkitConfigurateAdaption;
import me.rickyb98.spaceannouncements.util.Configuration;
import org.bukkit.plugin.java.JavaPlugin;

public class SpaceAnnouncementsBukkit extends JavaPlugin {

    private static SpaceAnnouncementsBukkit instance;

    public SpaceAnnouncementsBukkit() {
        instance = this;
    }

    public static SpaceAnnouncementsBukkit getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        super.onEnable();

        // todo add metrics

        BukkitConfig config = new BukkitConfig(SpaceAnnouncementsBukkit.getInstance(), "config.yml");
        SpaceAnnouncements instance = new SpaceAnnouncements(
                new BukkitPlugin(SpaceAnnouncementsBukkit.getInstance()),
                new Configuration(new BukkitConfigurateAdaption(SpaceAnnouncementsBukkit.getInstance(), config)),
                new BukkitConfigurateAdaption(this, new BukkitConfig(this, "announcements.yml"))
        );
        SpaceAnnouncements.setInstance(instance);
        instance.enable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
