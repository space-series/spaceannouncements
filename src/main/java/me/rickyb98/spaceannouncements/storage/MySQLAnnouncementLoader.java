package me.rickyb98.spaceannouncements.storage;

import com.yakovliam.spaceapi.config.adapter.ConfigurationAdapter;
import me.rickyb98.spaceannouncements.SpaceAnnouncements;
import me.rickyb98.spaceannouncements.handler.Announcement;
import me.rickyb98.spaceannouncements.util.MySQLStorage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static me.rickyb98.spaceannouncements.util.Configuration.*;

public class MySQLAnnouncementLoader extends AbstractAnnouncementLoader {

    private static MySQLAnnouncementLoader instance;

    public static MySQLAnnouncementLoader getInstance() {
        if (instance == null)
            instance = new MySQLAnnouncementLoader();
        return instance;
    }

    private MySQLStorage storage;

    public MySQLAnnouncementLoader() {
        ConfigurationAdapter adapter = Objects.requireNonNull(SpaceAnnouncements.getInstance().getConfiguration().getAdapter());
        storage = new MySQLStorage(
                DB_HOSTNAME.get(adapter),
                DB_PORT.get(adapter),
                DB_USERNAME.get(adapter),
                DB_PASSWORD.get(adapter),
                DB_NAME.get(adapter)
        );
    }

    @Override
    public Set<Announcement> loadAnnouncements() {
        Set<Announcement> announcementSet = new HashSet<>();
        try (Connection connection = storage.getConnection()) {
            initTables(connection);
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery("SELECT * FROM space_announcements");

                while (resultSet.next()) {
                    String handle = resultSet.getString("handle");
                    String textMessage = resultSet.getString("textMessage");
                    boolean isGlobal = resultSet.getBoolean("isGlobal");
                    String serverNames = resultSet.getString("servers");
                    String[] servers;
                    if (resultSet.wasNull())
                        servers = new String[]{};
                    else
                        servers = serverNames.replaceAll("\\s", "").split(",");

                    announcementSet.add(new Announcement(handle, textMessage, isGlobal, servers));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return announcementSet;
    }

    private void initTables(Connection connection) {
        // todo check if tables exist, otherwise create
    }
}
