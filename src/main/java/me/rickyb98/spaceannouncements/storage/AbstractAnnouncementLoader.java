package me.rickyb98.spaceannouncements.storage;

import me.rickyb98.spaceannouncements.handler.Announcement;

import java.util.Set;

public abstract class AbstractAnnouncementLoader {

    public abstract Set<Announcement> loadAnnouncements();

    public enum Type {
        FILE,
        MYSQL
    }

}
