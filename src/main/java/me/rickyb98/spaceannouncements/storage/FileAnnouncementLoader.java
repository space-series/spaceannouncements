package me.rickyb98.spaceannouncements.storage;

import com.google.common.base.Joiner;
import com.yakovliam.spaceapi.config.adapter.ConfigurationAdapter;
import me.rickyb98.spaceannouncements.handler.Announcement;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class FileAnnouncementLoader extends AbstractAnnouncementLoader {

    private final ConfigurationAdapter adapter;

    public FileAnnouncementLoader(ConfigurationAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public Set<Announcement> loadAnnouncements() {
        adapter.reload();
        Set<String> keys = adapter.getKeys("announcements", new HashSet<>());
        Set<Announcement> announcements = new HashSet<>();
        for (String key : keys) {
            announcements.add(new Announcement(
                    key,
                    Joiner.on("\n").join(adapter.getStringList(key + ".message", Collections.singletonList(""))),
                    adapter.getBoolean(key + ".global", false),
                    adapter.getStringList(key + ".servers", Collections.emptyList()).toArray(new String[]{})
            ));
        }

        return null;
    }
}
